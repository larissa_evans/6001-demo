﻿using System;
using System.Collections.Generic;

namespace demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in your name and press <ENETR>");
            var name = Console.ReadLine();
            //Console.WriteLine("Please type in your age and press <ENETR>");
            //var age = Console.ReadLine();

            //Console.WriteLine("Hi my name is " + name + " and i am " + age + " years old");
            //Console.WriteLine("Hi my name is {0} and i am {1} years old", name, age);
            //Console.WriteLine($"hi my name is {name} and i am {age} years old"); // string interpolation 

            //if statement structure
            //if(name == "Scotty")
            //{
            //    Console.WriteLine("You guessed my name!");
            //}
            //else
            //{
            //    Console.WriteLine("You guessed wrong!");
            //}

            //array
            //var fruits = new string[3] { "tomato", "oranges", "peaches" };

            //string join  - adds everything on a single line
            //Console.WriteLine(string.Join(", ", fruits));

            //LOOPS

            //looping through fruit and writing line to screen until no strings left
            //for(var i =0; i< fruits.Length; i++)
            //{
            //    //e.g. fruits[2];
            //    Console.WriteLine(fruits[i]);
            //}

            //foreach(var x in fruits)
            //{
            //    Console.WriteLine(x);
            //}

            //Console.WriteLine($"Please select a fruit that you like by typing a number");
            ////array
            //var fruits = new string[3] { "tomato", "oranges", "peaches" };

            ////LOOPS

            ////looping through fruit and writing line to screen until no strings left
            //for (var i = 0; i < fruits.Length; i++)
            //{
            //    //e.g. fruits[2];
            //    Console.WriteLine($"{i} - {fruits[i]}");
            //}

            //var selection = int.Parse(Console.ReadLine());
            //Console.WriteLine($"Hi i am {name} and i like {fruits[selection]}");

            Console.ReadLine();

        }

        public static void fruitselection()
        {
            Console.WriteLine("Please type in your name and press <ENETR>");
            var name = Console.ReadLine();

            Console.WriteLine($"Please select a fruit that you like by typing a number");
            //array
            var fruits = new string[3] { "tomato", "oranges", "peaches" };

            //LOOPS

            //looping through fruit and writing line to screen until no strings left
            for (var i = 0; i < fruits.Length; i++)
            {
                //e.g. fruits[2];
                Console.WriteLine($"{i} - {fruits[i]}");
            }

            var selection = int.Parse(Console.ReadLine());
            Console.WriteLine($"Hi i am {name} and i like {fruits[selection]}");
        }

    }
}
